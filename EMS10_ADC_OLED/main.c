#include <msp430.h> 
#include <stdint.h>

#pragma vector = ADC10_VECTOR
__interrupt void adc10 (void){
    int a = ( ADC10MEM /1023.0)*1500.0;
    setTemp(a);
    ADC10CTL0 &= ~(ADC10IFG);
}
/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weergeven
 * temperatuur aan.
 *
 * int temp: De te weergeven temperatuur.
 * Max is 999 min is -99. Hierbuiten weer-
 * geeft het "Err-"
 *
 * Voorbeeld:
 * setTemp(100); //stel 10.0 graden in
 */
void setTemp(int temp);

/* Deze functie past de bovenste titel aan.
 * char tekst[]: de te weergeven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); //laat Hallo zien
 */
void setTitle(char tekst[]);

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    //stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */

    initDisplay();
    setTitle("Opdracht7.1.13");

    uint8_t temperatuur=0;

    ADC10CTL1 = INCH_5 | SHS_0 | ADC10DIV_4 | ADC10SSEL_0 | CONSEQ_0 ;
    ADC10CTL0 = SREF_1 | ADC10SHT_0 | ADC10SR | REFOUT | REFBURST |  REFON | ADC10ON | ENC | ADC10IE;


    __enable_interrupt();

    while(1)
    {
        ADC10CTL0 |= ADC10SC;
        __delay_cycles(4000000);
    }
}
